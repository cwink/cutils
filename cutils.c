#include "include/cutils.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main (void)
{
  CUTILS_STRING *string = NULL;
  CUTILS_BINARY *binary = NULL;

  char uchar_buffer[512] = { '\0' };
  char buffer[4] = { '\0' };
  int ch = 0;

  FILE *file = fopen ("assets/f_tests.txt", "rb");

  cutils_file_peek_string (file, buffer, 3);

  fprintf (stdout, "%s\n", buffer);

  ch = fgetc (file);

  fprintf (stdout, "%c\n", ch);

  fprintf (stdout, "%c\n", (char)cutils_file_peek_char (file));

  ch = fgetc (file);

  fprintf (stdout, "%c\n", ch);

  fclose (file);

  printf ("%ld\n", cutils_get_ticks ());

  string = cutils_base64_encode ((unsigned char *)"any carnal pleasure", 19);

  printf ("%s\n", cutils_string_get (string));

  cutils_string_destroy (&string);

  binary = cutils_base64_decode ("YW55IGNhcm5hbCBwbGVhc3VyZQ==");

  memcpy (uchar_buffer, (char *)cutils_binary_get (binary),
          cutils_binary_size (binary));

  printf ("%s\n", uchar_buffer);

  cutils_binary_destroy (&binary);

  return EXIT_SUCCESS;
}
