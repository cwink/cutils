#include "../include/cutils.h"

#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#ifdef CUTILS_OS_UNIX_LINUX_DARWIN
#include <sys/time.h>
#endif /* CUTILS_OS_UNIX_LINUX_DARWIN */

#ifdef CUTILS_OS_WINDOWS
#include <windows.h>
#endif /* CUTILS_OS_WINDOWS */

#define CUTILS_ERROR_MESSAGE_MAX_SIZE 4096

static const char base64_lookup_encode[]
    = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/' };

static const char base64_lookup_decode[]
    = { 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,  0,  0,  0,  0,  62, 0,  0,  0,  63, 52, 53, 54, 55, 56, 57,
        58, 59, 60, 61, 0,  0,  0,  0,  0,  0,  0,  0,  1,  2,  3,  4,  5,  6,
        7,  8,  9,  10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
        25, 0,  0,  0,  0,  0,  0,  26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
        37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51 };

static char cutils_error_message[CUTILS_ERROR_MESSAGE_MAX_SIZE] = { '\0' };

struct cutils_string_t
{
  char *data;
  size_t capacity;
};

struct cutils_binary_t
{
  unsigned char *data;
  size_t size;
  size_t capacity;
};

struct cutils_list_t
{
  void *value;
  struct cutils_list_t *next;
  struct cutils_list_t *prev;
  CUTILS_LIST_DESTRUCTOR destructor;
  CUTILS_LIST_COPIER copier;
  CUTILS_LIST_COMPARATOR comparator;
};

struct cutils_map_t
{
  void *value;
  CUTILS_STRING *key;
  struct cutils_map_t *next;
  struct cutils_map_t *prev;
  CUTILS_LIST_DESTRUCTOR destructor;
  CUTILS_LIST_COPIER copier;
  CUTILS_LIST_COMPARATOR comparator;
};

static void
cutils_error_set (const char *const fmt, ...)
{
  va_list args;

  if (fmt == NULL)
    {
      return;
    }

  va_start (args, fmt);

  memset (cutils_error_message, '\0', CUTILS_ERROR_MESSAGE_MAX_SIZE);

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
  vsprintf (cutils_error_message, fmt, args);
#pragma GCC diagnostic pop

  va_end (args);
}

static void
cutils_error_clear (void)
{
  memset (cutils_error_message, '\0', CUTILS_ERROR_MESSAGE_MAX_SIZE);
}

static CUTILS_BOOL
cutils_string_increase_capacity (CUTILS_STRING **string)
{
  CUTILS_STRING *new_string = NULL;

  cutils_error_clear ();

  if (string == NULL || *string == NULL)
    {
      cutils_error_set ("Arguments 'string' or '*string' are NULL for "
                        "cutils_string_increase_capacity.");

      return CUTILS_BOOL_FALSE;
    }

  new_string = malloc (sizeof (CUTILS_STRING));

  if (new_string == NULL)
    {
      cutils_error_set ("Failed to allocate memory for 'new_string' in "
                        "cutils_string_increase_capacity.");

      return CUTILS_BOOL_FALSE;
    }

  memset (new_string, 0, sizeof (CUTILS_STRING));

  new_string->capacity = (*string)->capacity * (*string)->capacity;

  new_string->data = malloc (new_string->capacity);

  if (new_string->data == NULL)
    {
      cutils_error_set ("Failed to allocate memory for 'new_string->data' in "
                        "cutils_string_increase_capacity.");

      return CUTILS_BOOL_FALSE;
    }

  memset (new_string->data, '\0', new_string->capacity);

  strcpy (new_string->data, (*string)->data);

  cutils_string_destroy (string);

  *string = new_string;

  return CUTILS_BOOL_TRUE;
}

static CUTILS_BOOL
cutils_binary_increase_capacity (CUTILS_BINARY **binary)
{
  CUTILS_BINARY *new_binary = NULL;

  cutils_error_clear ();

  if (binary == NULL || *binary == NULL)
    {
      cutils_error_set ("Arguments 'binary' or '*binary' are NULL for "
                        "cutils_binary_increase_capacity.");

      return CUTILS_BOOL_FALSE;
    }

  new_binary = malloc (sizeof (CUTILS_BINARY));

  if (new_binary == NULL)
    {
      cutils_error_set ("Failed to allocate memory for 'new_binary' in "
                        "cutils_binary_increase_capacity.");

      return CUTILS_BOOL_FALSE;
    }

  memset (new_binary, 0, sizeof (CUTILS_BINARY));

  new_binary->capacity = (*binary)->capacity * (*binary)->capacity;

  new_binary->size = (*binary)->size;

  new_binary->data = malloc (sizeof (unsigned char) * new_binary->capacity);

  if (new_binary->data == NULL)
    {
      cutils_error_set ("Failed to allocate memory for 'new_binary->data' in "
                        "cutils_binary_increase_capacity.");

      return CUTILS_BOOL_FALSE;
    }

  memset (new_binary->data, '\0', new_binary->capacity);

  memcpy (new_binary->data, (*binary)->data, (*binary)->size);

  cutils_binary_destroy (binary);

  *binary = new_binary;

  return CUTILS_BOOL_TRUE;
}

const char *
cutils_error_get (void)
{
  return cutils_error_message;
}

CUTILS_BOOL
cutils_error_triggered (void)
{
  if (strcmp (cutils_error_message, "") == 0)
    {
      return CUTILS_BOOL_TRUE;
    }

  return CUTILS_BOOL_FALSE;
}

CUTILS_STRING *
cutils_string_create (void)
{
  CUTILS_STRING *string = NULL;

  cutils_error_clear ();

  string = malloc (sizeof (CUTILS_STRING));

  if (string == NULL)
    {
      cutils_error_set (
          "Failed to allocate memory for 'string' in cutils_string_create.");

      return NULL;
    }

  memset (string, 0, sizeof (CUTILS_STRING));

  string->capacity = 2;

  string->data = malloc (string->capacity);

  if (string->data == NULL)
    {
      cutils_error_set ("Failed to allocate memory for 'string->data' in "
                        "cutils_string_create.");

      free (string);

      return NULL;
    }

  memset (string->data, '\0', string->capacity);

  return string;
}

CUTILS_STRING *
cutils_string_create_from_text (const char *const text)
{
  CUTILS_STRING *string = NULL;

  cutils_error_clear ();

  if (text == NULL)
    {
      cutils_error_set (
          "Argument 'text' for cutils_string_create_from_text is NULL.");

      return NULL;
    }

  string = malloc (sizeof (CUTILS_STRING));

  if (string == NULL)
    {
      cutils_error_set ("Failed to allocate memory for 'string' in "
                        "cutils_string_create_from_text.");

      return NULL;
    }

  memset (string, 0, sizeof (CUTILS_STRING));

  string->capacity = strlen (text) + 1;

  string->data = malloc (string->capacity);

  if (string->data == NULL)
    {
      cutils_error_set ("Failed to allocate memory for 'string->data' in "
                        "cutils_string_create_from_text.");
      free (string);

      return NULL;
    }

  memset (string->data, '\0', string->capacity);

  strcpy (string->data, text);

  return string;
}

void
cutils_string_destroy (CUTILS_STRING **string)
{
  cutils_error_clear ();

  if (string == NULL || *string == NULL)
    {
      return;
    }

  if ((*string)->data != NULL)
    {
      memset ((*string)->data, '\0', (*string)->capacity);

      free ((*string)->data);
    }

  memset (*string, 0, sizeof (CUTILS_STRING));

  free (*string);

  *string = NULL;
}

CUTILS_STRING *
cutils_string_clone (CUTILS_STRING *string)
{
  cutils_error_clear ();

  if (string == NULL)
    {
      cutils_error_set ("Argument 'string' in cutils_string_clone is NULL.");

      return NULL;
    }

  if (string->data == NULL)
    {
      cutils_error_set (
          "Argument 'string->data' in cutils_string_clone is NULL.");

      return NULL;
    }

  return cutils_string_create_from_text (string->data);
}

CUTILS_BOOL
cutils_string_insert (CUTILS_STRING **string, const size_t index,
                      const char value)
{
  CUTILS_STRING *clone = NULL;

  size_t i = 0;

  cutils_error_clear ();

  if (string == NULL)
    {
      cutils_error_set ("Argument 'string' in cutils_string_insert is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (*string == NULL)
    {
      cutils_error_set ("Argument '*string' in cutils_string_insert is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if ((*string)->data == NULL)
    {
      cutils_error_set (
          "Argument 'string->data' in cutils_string_insert is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (index > strlen ((*string)->data) + 1)
    {
      cutils_error_set (
          "Argument 'index' [%ld] is out of bounds in cutils_string_insert.",
          index);

      return CUTILS_BOOL_FALSE;
    }

  if (strlen ((*string)->data) + 1 >= (*string)->capacity)
    {
      if (cutils_string_increase_capacity (string) != CUTILS_BOOL_TRUE)
        {
          return CUTILS_BOOL_FALSE;
        }
    }

  clone = cutils_string_clone (*string);

  if (clone == NULL)
    {
      return CUTILS_BOOL_FALSE;
    }

  for (i = index; i < strlen (clone->data); ++i)
    {
      (*string)->data[i + 1] = clone->data[i];
    }

  (*string)->data[index] = value;

  cutils_string_destroy (&clone);

  return CUTILS_BOOL_TRUE;
}

CUTILS_BOOL
cutils_string_append (CUTILS_STRING **string, const char value)
{
  cutils_error_clear ();

  if (string == NULL)
    {
      cutils_error_set ("Argument 'string' in cutils_string_append is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (*string == NULL)
    {
      cutils_error_set ("Argument '*string' in cutils_string_append is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if ((*string)->data == NULL)
    {
      cutils_error_set (
          "Argument 'string->data' in cutils_string_append is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  return cutils_string_insert (string, strlen ((*string)->data), value);
}

CUTILS_BOOL
cutils_string_prepend (CUTILS_STRING **string, const char value)
{
  cutils_error_clear ();

  return cutils_string_insert (string, 0, value);
}

CUTILS_BOOL
cutils_string_erase (CUTILS_STRING **string, const size_t index)
{
  CUTILS_STRING *clone = NULL;

  size_t i = 0;

  cutils_error_clear ();

  if (string == NULL)
    {
      cutils_error_set ("Argument 'string' in cutils_string_erase is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (*string == NULL)
    {
      cutils_error_set ("Argument '*string' in cutils_string_erase is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if ((*string)->data == NULL)
    {
      cutils_error_set (
          "Argument 'string->data' in cutils_string_erase is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (index > strlen ((*string)->data) + 1)
    {
      cutils_error_set (
          "Argument 'index' [%ld] is out of bounds in cutils_string_erase.",
          index);

      return CUTILS_BOOL_FALSE;
    }

  if (strlen ((*string)->data) + 1 >= (*string)->capacity)
    {
      if (cutils_string_increase_capacity (string) != CUTILS_BOOL_TRUE)
        {
          return CUTILS_BOOL_FALSE;
        }
    }

  clone = cutils_string_clone (*string);

  if (clone == NULL)
    {
      return CUTILS_BOOL_FALSE;
    }

  for (i = index; i < strlen (clone->data); ++i)
    {
      (*string)->data[i] = clone->data[i + 1];
    }

  (*string)->data[strlen (clone->data)] = '\0';

  cutils_string_destroy (&clone);

  return CUTILS_BOOL_TRUE;
}

const char *
cutils_string_get (CUTILS_STRING *string)
{
  cutils_error_clear ();

  if (string == NULL)
    {
      cutils_error_set ("Argument 'string' in cutils_string_get is NULL.");

      return NULL;
    }

  if (string->data == NULL)
    {
      cutils_error_set (
          "Argument 'string->data' in cutils_string_get is NULL.");

      return NULL;
    }

  return string->data;
}

CUTILS_BOOL
cutils_string_reset (CUTILS_STRING **string, const char *const text)
{
  CUTILS_STRING *new_string = NULL;

  cutils_error_clear ();

  if (string == NULL)
    {
      cutils_error_set ("Argument 'string' in cutils_string_reset is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (*string == NULL)
    {
      cutils_error_set ("Argument '*string' in cutils_string_reset is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if ((*string)->data == NULL)
    {
      cutils_error_set (
          "Argument 'string->data' in cutils_string_reset is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (text == NULL)
    {
      cutils_error_set ("Argument 'text' in cutils_string_reset is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  new_string = cutils_string_create_from_text (text);

  if (new_string == NULL)
    {
      return CUTILS_BOOL_FALSE;
    }

  cutils_string_destroy (string);

  *string = new_string;

  return CUTILS_BOOL_TRUE;
}

size_t
cutils_string_length (CUTILS_STRING *string)
{
  cutils_error_clear ();

  if (string == NULL)
    {
      cutils_error_set ("Argument 'string' in cutils_string_length is NULL.");

      return 0;
    }

  if (string->data == NULL)
    {
      cutils_error_set (
          "Argument 'string->data' in cutils_string_length is NULL.");

      return 0;
    }

  return strlen (string->data);
}

CUTILS_BOOL
cutils_string_compare (const CUTILS_STRING *const left,
                       const CUTILS_STRING *const right)
{
  if (left == NULL)
    {
      cutils_error_set ("Argument 'left' in cutils_string_compare is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (right == NULL)
    {
      cutils_error_set ("Argument 'right' in cutils_string_compare is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (left->data == NULL)
    {
      cutils_error_set (
          "Argument 'left->data' in cutils_string_compare is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (right->data == NULL)
    {
      cutils_error_set (
          "Argument 'right->data' in cutils_string_compare is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (strcmp (left->data, right->data) == 0)
    {
      return CUTILS_BOOL_TRUE;
    }

  return CUTILS_BOOL_FALSE;
}

CUTILS_BOOL
cutils_string_compare_with_text (const CUTILS_STRING *const left,
                                 const char *const right)
{
  if (left == NULL)
    {
      cutils_error_set (
          "Argument 'left' in cutils_string_compare_with_text is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (right == NULL)
    {
      cutils_error_set (
          "Argument 'right' in cutils_string_compare_with_text is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (left->data == NULL)
    {
      cutils_error_set (
          "Argument 'left->data' in cutils_string_compare_with_text is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (strcmp (left->data, right) == 0)
    {
      return CUTILS_BOOL_TRUE;
    }

  return CUTILS_BOOL_FALSE;
}

CUTILS_BINARY *
cutils_binary_create (void)
{
  CUTILS_BINARY *binary = NULL;

  cutils_error_clear ();

  binary = malloc (sizeof (CUTILS_BINARY));

  if (binary == NULL)
    {
      cutils_error_set (
          "Failed to allocate memory for 'binary' in cutils_binary_create.");

      return NULL;
    }

  memset (binary, 0, sizeof (CUTILS_BINARY));

  binary->capacity = 2;

  binary->size = 0;

  binary->data = malloc (sizeof (unsigned char) * binary->capacity);

  if (binary->data == NULL)
    {
      cutils_error_set ("Failed to allocate memory for 'binary->data' in "
                        "cutils_binary_create.");

      free (binary);

      return NULL;
    }

  memset (binary->data, 0, sizeof (unsigned char) * binary->capacity);

  return binary;
}

CUTILS_BINARY *
cutils_binary_create_from_data (const unsigned char *const data,
                                const size_t size)
{
  CUTILS_BINARY *binary = NULL;

  cutils_error_clear ();

  if (data == NULL)
    {
      cutils_error_set (
          "Argument 'data' for cutils_binary_create_from_data is NULL.");

      return NULL;
    }

  binary = malloc (sizeof (CUTILS_BINARY));

  if (binary == NULL)
    {
      cutils_error_set ("Failed to allocate memory for 'binary' in "
                        "cutils_binary_create_from_data.");

      return NULL;
    }

  memset (binary, 0, sizeof (CUTILS_BINARY));

  binary->capacity = size + 1;

  binary->size = size;

  binary->data = malloc (sizeof (unsigned char) * binary->capacity);

  if (binary->data == NULL)
    {
      cutils_error_set ("Failed to allocate memory for 'binary->data' in "
                        "cutils_binary_create_from_data.");
      free (binary);

      return NULL;
    }

  memset (binary->data, 0, sizeof (unsigned char) * binary->capacity);

  memcpy (binary->data, data, size);

  return binary;
}

void
cutils_binary_destroy (CUTILS_BINARY **binary)
{
  cutils_error_clear ();

  if (binary == NULL || *binary == NULL)
    {
      return;
    }

  if ((*binary)->data != NULL)
    {
      memset ((*binary)->data, 0,
              sizeof (unsigned char) * (*binary)->capacity);

      free ((*binary)->data);
    }

  memset (*binary, 0, sizeof (CUTILS_BINARY));

  free (*binary);

  *binary = NULL;
}

CUTILS_BINARY *
cutils_binary_clone (CUTILS_BINARY *binary)
{
  cutils_error_clear ();

  if (binary == NULL)
    {
      cutils_error_set ("Argument 'binary' in cutils_binary_clone is NULL.");

      return NULL;
    }

  if (binary->data == NULL)
    {
      cutils_error_set (
          "Argument 'binary->data' in cutils_binary_clone is NULL.");

      return NULL;
    }

  return cutils_binary_create_from_data (binary->data, binary->size);
}

CUTILS_BOOL
cutils_binary_insert (CUTILS_BINARY **binary, const size_t index,
                      const unsigned char value)
{
  CUTILS_BINARY *clone = NULL;

  size_t i = 0;

  cutils_error_clear ();

  if (binary == NULL)
    {
      cutils_error_set ("Argument 'binary' in cutils_binary_insert is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (*binary == NULL)
    {
      cutils_error_set ("Argument '*binary' in cutils_binary_insert is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if ((*binary)->data == NULL)
    {
      cutils_error_set (
          "Argument 'binary->data' in cutils_binary_insert is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (index > (*binary)->size + 1)
    {
      cutils_error_set (
          "Argument 'index' [%ld] is out of bounds in cutils_binary_insert.",
          index);

      return CUTILS_BOOL_FALSE;
    }

  if ((*binary)->size + 1 >= sizeof (unsigned char) * (*binary)->capacity)
    {
      if (cutils_binary_increase_capacity (binary) != CUTILS_BOOL_TRUE)
        {
          return CUTILS_BOOL_FALSE;
        }
    }

  clone = cutils_binary_clone (*binary);

  if (clone == NULL)
    {
      return CUTILS_BOOL_FALSE;
    }

  for (i = index; i < clone->size; ++i)
    {
      (*binary)->data[i + 1] = clone->data[i];
    }

  (*binary)->data[index] = value;

  ++(*binary)->size;

  cutils_binary_destroy (&clone);

  return CUTILS_BOOL_TRUE;
}

CUTILS_BOOL
cutils_binary_append (CUTILS_BINARY **binary, const unsigned char value)
{
  cutils_error_clear ();

  if (binary == NULL)
    {
      cutils_error_set ("Argument 'binary' in cutils_binary_append is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (*binary == NULL)
    {
      cutils_error_set ("Argument '*binary' in cutils_binary_append is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if ((*binary)->data == NULL)
    {
      cutils_error_set (
          "Argument 'binary->data' in cutils_binary_append is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  return cutils_binary_insert (binary, (*binary)->size, value);
}

CUTILS_BOOL
cutils_binary_prepend (CUTILS_BINARY **binary, const unsigned char value)
{
  cutils_error_clear ();

  return cutils_binary_insert (binary, 0, value);
}

CUTILS_BOOL
cutils_binary_erase (CUTILS_BINARY **binary, const size_t index)
{
  CUTILS_BINARY *clone = NULL;

  size_t i = 0;

  cutils_error_clear ();

  if (binary == NULL)
    {
      cutils_error_set ("Argument 'binary' in cutils_binary_erase is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (*binary == NULL)
    {
      cutils_error_set ("Argument '*binary' in cutils_binary_erase is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if ((*binary)->data == NULL)
    {
      cutils_error_set (
          "Argument 'binary->data' in cutils_binary_erase is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (index > (*binary)->size + 1)
    {
      cutils_error_set (
          "Argument 'index' [%ld] is out of bounds in cutils_binary_erase.",
          index);

      return CUTILS_BOOL_FALSE;
    }

  if ((*binary)->size + 1 >= (*binary)->capacity)
    {
      if (cutils_binary_increase_capacity (binary) != CUTILS_BOOL_TRUE)
        {
          return CUTILS_BOOL_FALSE;
        }
    }

  clone = cutils_binary_clone (*binary);

  if (clone == NULL)
    {
      return CUTILS_BOOL_FALSE;
    }

  for (i = index; i < clone->size; ++i)
    {
      (*binary)->data[i] = clone->data[i + 1];
    }

  (*binary)->data[clone->size] = 0;

  cutils_binary_destroy (&clone);

  return CUTILS_BOOL_TRUE;
}

const unsigned char *
cutils_binary_get (CUTILS_BINARY *binary)
{
  cutils_error_clear ();

  if (binary == NULL)
    {
      cutils_error_set ("Argument 'binary' in cutils_binary_get is NULL.");

      return NULL;
    }

  if (binary->data == NULL)
    {
      cutils_error_set (
          "Argument 'binary->data' in cutils_binary_get is NULL.");

      return NULL;
    }

  return binary->data;
}

CUTILS_BOOL
cutils_binary_reset (CUTILS_BINARY **binary, const unsigned char *const data,
                     const size_t size)
{
  CUTILS_BINARY *new_binary = NULL;

  cutils_error_clear ();

  if (binary == NULL)
    {
      cutils_error_set ("Argument 'binary' in cutils_binary_reset is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (*binary == NULL)
    {
      cutils_error_set ("Argument '*binary' in cutils_binary_reset is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if ((*binary)->data == NULL)
    {
      cutils_error_set (
          "Argument 'binary->data' in cutils_binary_reset is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (data == NULL)
    {
      cutils_error_set ("Argument 'text' in cutils_binary_reset is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  new_binary = cutils_binary_create_from_data (data, size);

  if (new_binary == NULL)
    {
      return CUTILS_BOOL_FALSE;
    }

  cutils_binary_destroy (binary);

  *binary = new_binary;

  return CUTILS_BOOL_TRUE;
}

size_t
cutils_binary_size (CUTILS_BINARY *binary)
{
  cutils_error_clear ();

  if (binary == NULL)
    {
      cutils_error_set ("Argument 'binary' in cutils_binary_size is NULL.");

      return 0;
    }

  if (binary->data == NULL)
    {
      cutils_error_set (
          "Argument 'binary->data' in cutils_binary_size is NULL.");

      return 0;
    }

  return binary->size;
}

CUTILS_BOOL
cutils_binary_compare (const CUTILS_BINARY *const left,
                       const CUTILS_BINARY *const right)
{
  if (left == NULL)
    {
      cutils_error_set ("Argument 'left' in cutils_binary_compare is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (right == NULL)
    {
      cutils_error_set ("Argument 'right' in cutils_binary_compare is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (left->data == NULL)
    {
      cutils_error_set (
          "Argument 'left->data' in cutils_binary_compare is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (right->data == NULL)
    {
      cutils_error_set (
          "Argument 'right->data' in cutils_binary_compare is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (right->size == left->size
      && memcmp (left->data, right->data, left->size) == 0)
    {
      return CUTILS_BOOL_TRUE;
    }

  return CUTILS_BOOL_FALSE;
}

CUTILS_BOOL
cutils_binary_compare_with_data (const CUTILS_BINARY *const left,
                                 const unsigned char *const right,
                                 const size_t size)
{
  if (left == NULL)
    {
      cutils_error_set (
          "Argument 'left' in cutils_binary_compare_with_data is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (right == NULL)
    {
      cutils_error_set (
          "Argument 'right' in cutils_binary_compare_with_data is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (left->data == NULL)
    {
      cutils_error_set (
          "Argument 'left->data' in cutils_binary_compare_with_data is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (left->size == size && memcmp (left->data, right, size) == 0)
    {
      return CUTILS_BOOL_TRUE;
    }

  return CUTILS_BOOL_FALSE;
}

FILE *
cutils_file_open (const char *const file_name, const CUTILS_FILE_OPEN type)
{
  FILE *file = NULL;

  cutils_error_clear ();

  if (file_name == NULL)
    {
      cutils_error_set ("Argument 'file_open' for cutils_file_open is NULL.");

      return NULL;
    }

  if (type >= CUTILS_FILE_OPEN_MAX)
    {
      cutils_error_set (
          "Argument 'type' for cutils_file_open is an invalid enum.");

      return NULL;
    }

  switch (type)
    {
    case CUTILS_FILE_OPEN_READ:
      file = fopen (file_name, "rb");

      break;
    case CUTILS_FILE_OPEN_WRITE:
      file = fopen (file_name, "wb");

      break;
    case CUTILS_FILE_OPEN_APPEND:
      file = fopen (file_name, "ab");

      break;
    case CUTILS_FILE_OPEN_MAX:
      break;
    }

  if (file == NULL || ferror (file) != 0)
    {
      cutils_error_set ("Failed to open file '%s' in cutils_file_open.",
                        file_name);

      return NULL;
    }

  return file;
}

CUTILS_BOOL
cutils_file_close (FILE **file)
{
  cutils_error_clear ();

  if (file == NULL)
    {
      cutils_error_set ("Argument 'file' in cutils_file_close is NULL");

      return CUTILS_BOOL_FALSE;
    }

  if (*file == NULL)
    {
      cutils_error_set ("Argument '*file' in cutils_file_close is NULL");

      return CUTILS_BOOL_FALSE;
    }

  if (fclose (*file) != 0)
    {
      cutils_error_set ("Failed to close file in cutils_file_close.");

      return CUTILS_BOOL_FALSE;
    }

  return CUTILS_BOOL_TRUE;
}

int
cutils_file_get_char (FILE *file)
{
  int ch = 0;

  cutils_error_clear ();

  if (file == NULL)
    {
      cutils_error_set ("Argument 'file' for cutils_file_get_char is NULL.");

      return EOF;
    }

  ch = fgetc (file);

  if (ferror (file) != 0)
    {
      cutils_error_set ("Failed to read character from file stream in "
                        "cutils_file_get_char.");

      return EOF;
    }

  return ch;
}

CUTILS_BOOL
cutils_file_get_string (FILE *file, char *buffer, const size_t count)
{
  int ch = 0;

  size_t index = 0;

  cutils_error_clear ();

  if (file == NULL)
    {
      cutils_error_set ("Argument 'file' for cutils_file_get_string is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (buffer == NULL)
    {
      cutils_error_set (
          "Argument 'buffer' for cutils_file_get_string is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  memset (buffer, '\0', count);

  for (index = 0; index < count; ++index)
    {
      ch = fgetc (file);

      if (ferror (file) != 0)
        {
          cutils_error_set ("An error occured retrieving character from file "
                            "stream in cutils_file_get_string.");

          memset (buffer, '\0', count);

          return CUTILS_BOOL_FALSE;
        }

      if (ch == EOF)
        {
          cutils_error_set (
              "Reached the end of file stream in cutils_file_get_string.");

          memset (buffer, '\0', count);

          return CUTILS_BOOL_FALSE;
        }

      buffer[index] = (char)ch;
    }

  return CUTILS_BOOL_TRUE;
}

int
cutils_file_peek_char (FILE *file)
{
  int ch = 0;

  cutils_error_clear ();

  if (file == NULL)
    {
      cutils_error_set ("Argument 'file' for cutils_file_peek_char is NULL.");

      return EOF;
    }

  ch = fgetc (file);

  if (ferror (file) != 0)
    {
      cutils_error_set ("Failed to retrieve character from file stream in "
                        "cutils_file_peek_char.");

      return EOF;
    }

  if (fseek (file, -1, SEEK_CUR) != 0)
    {
      cutils_error_set ("Failed to return back to initial place in file "
                        "stream for cutils_file_peek_char.");

      return EOF;
    }

  return ch;
}

CUTILS_BOOL
cutils_file_peek_string (FILE *file, char *buffer, const size_t count)
{
  int ch = 0;

  size_t index = 0;

  cutils_error_clear ();

  if (file == NULL)
    {
      cutils_error_set (
          "Argument 'file' for cutils_file_peek_string is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (buffer == NULL)
    {
      cutils_error_set (
          "Argument 'buffer' for cutils_file_peek_string is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  memset (buffer, '\0', count);

  for (index = 0; index < count; ++index)
    {
      ch = fgetc (file);

      if (ferror (file) != 0)
        {
          cutils_error_set ("An error occured retrieving character from file "
                            "stream in cutils_file_peek_string.");

          memset (buffer, '\0', count);

          return CUTILS_BOOL_FALSE;
        }

      if (ch == EOF)
        {
          cutils_error_set (
              "Reached the end of file stream in cutils_file_peek_string.");

          memset (buffer, '\0', count);

          return CUTILS_BOOL_FALSE;
        }

      buffer[index] = (char)ch;
    }

  if (fseek (file, -1 * (int)count, SEEK_CUR) != 0)
    {
      cutils_error_set ("Failed to return back to initial place in file "
                        "stream for cutils_file_peek_string.");

      return CUTILS_BOOL_FALSE;
    }

  return CUTILS_BOOL_TRUE;
}

CUTILS_BOOL
cutils_file_put_char (FILE *file, const int ch)
{
  cutils_error_clear ();

  if (file == NULL)
    {
      cutils_error_set ("Argument 'file' in cutils_file_put_char is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  fputc (ch, file);

  if (ferror (file) != 0)
    {
      cutils_error_set (
          "Failed to place character '%c' into file in cutils_file_put_char.",
          (char)ch);

      return CUTILS_BOOL_FALSE;
    }

  return CUTILS_BOOL_TRUE;
}

CUTILS_BOOL
cutils_file_put_string (FILE *file, const char *const string)
{
  cutils_error_clear ();

  if (file == NULL)
    {
      cutils_error_set ("Argument 'file' in cutils_file_put_string is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (string == NULL)
    {
      cutils_error_set (
          "Argument 'string' in cutils_file_put_string is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  fputs (string, file);

  if (ferror (file) != 0)
    {
      cutils_error_set (
          "Failed to place string '%s' into file in cutils_file_put_string.",
          string);

      return CUTILS_BOOL_FALSE;
    }

  return CUTILS_BOOL_TRUE;
}

CUTILS_BOOL
cutils_file_read (FILE *file, void *buffer, const size_t size,
                  const size_t count)
{
  cutils_error_clear ();

  if (file == NULL)
    {
      cutils_error_set ("Argument 'file' in cutils_file_read is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (buffer == NULL)
    {
      cutils_error_set ("Argument 'buffer' in cutils_file_read is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  fread (buffer, size, count, file);

  if (ferror (file) != 0)
    {
      cutils_error_set (
          "Failed to read from file stream in cutils_file_read.");

      return CUTILS_BOOL_FALSE;
    }

  return CUTILS_BOOL_TRUE;
}

CUTILS_BOOL
cutils_file_write (FILE *file, const void *const buffer, const size_t size,
                   const size_t count)
{
  cutils_error_clear ();

  if (file == NULL)
    {
      cutils_error_set ("Argument 'file' in cutils_file_write is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (buffer == NULL)
    {
      cutils_error_set ("Argument 'buffer' in cutils_file_write is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  fwrite (buffer, size, count, file);

  if (ferror (file) != 0)
    {
      cutils_error_set (
          "Failed to write to file stream in cutils_file_write.");

      return CUTILS_BOOL_FALSE;
    }

  return CUTILS_BOOL_TRUE;
}

size_t
cutils_file_size (FILE *file)
{
  size_t size = 0;
  size_t original = 0;

  cutils_error_clear ();

  if (file == NULL)
    {
      cutils_error_set ("Argument 'file' in cutils_file_size is NULL.");

      return 0;
    }

  errno = 0;

  original = (size_t)ftell (file);

  if (errno > 0)
    {
      cutils_error_set (
          "Failed to retrieve current file position in cutils_file_size.");

      return 0;
    }

  if (fseek (file, 0, SEEK_END) != 0)
    {
      cutils_error_set (
          "Failed to set file position to end of file in cutils_file_size.");

      return 0;
    }

  size = (size_t)ftell (file);

  if (errno > 0)
    {
      cutils_error_set (
          "Failed to retrieve current file position in cutils_file_size.");

      return 0;
    }

  if (fseek (file, (long)original, SEEK_SET) != 0)
    {
      cutils_error_set ("Failed to set file position back to its original "
                        "position in cutils_file_size.");

      return 0;
    }

  return size;
}

CUTILS_LIST *
cutils_list_create (const CUTILS_LIST_DESTRUCTOR destructor,
                    const CUTILS_LIST_COPIER copier,
                    const CUTILS_LIST_COMPARATOR comparator)
{
  CUTILS_LIST *list = NULL;

  cutils_error_clear ();

  if (destructor == NULL)
    {
      cutils_error_set (
          "Argument 'destructor' in cutils_list_create is NULL.");

      return NULL;
    }

  if (copier == NULL)
    {
      cutils_error_set ("Argument 'copier' in cutils_list_create is NULL.");

      return NULL;
    }

  if (comparator == NULL)
    {
      cutils_error_set (
          "Argument 'comparator' in cutils_list_create is NULL.");

      return NULL;
    }

  list = malloc (sizeof (CUTILS_LIST));

  if (list == NULL)
    {
      cutils_error_set (
          "Failed to allocate memory for list in cutils_list_create.");

      return NULL;
    }

  memset (list, 0, sizeof (CUTILS_LIST));

  list->destructor = destructor;

  list->copier = copier;

  list->comparator = comparator;

  return list;
}

void
cutils_list_destroy (CUTILS_LIST **list)
{
  CUTILS_LIST *ptr = NULL;

  cutils_error_clear ();

  if (list == NULL || *list == NULL)
    {
      return;
    }

  ptr = *list;

  while (ptr != NULL)
    {
      CUTILS_LIST *next = ptr->next;

      if (ptr->value != NULL)
        {
          ptr->destructor (&ptr->value);
        }

      memset (ptr, 0, sizeof (CUTILS_LIST));

      free (ptr);

      ptr = next;
    }

  *list = NULL;
}

CUTILS_LIST *
cutils_list_clone (const CUTILS_LIST *const list)
{
  const CUTILS_LIST *ptr_list = NULL;
  CUTILS_LIST *ptr_clone = NULL;
  CUTILS_LIST *clone = NULL;

  cutils_error_clear ();

  if (list == NULL)
    {
      cutils_error_set ("Argument 'list' in cutils_list_clone is NULL.");

      return NULL;
    }

  if (list->prev != NULL)
    {
      cutils_error_set ("Argument 'list' is not a root node.");

      return NULL;
    }

  clone
      = cutils_list_create (list->destructor, list->copier, list->comparator);

  if (clone == NULL)
    {
      return NULL;
    }

  ptr_list = list;

  ptr_clone = clone;

  while (ptr_list != NULL)
    {
      clone->value = list->copier (list->value);

      if (clone->value == NULL)
        {
          cutils_error_set ("Cloned value is NULL in cutils_list_clone.");

          cutils_list_destroy (&clone);

          return NULL;
        }

      if (ptr_list->next != NULL)
        {
          clone->next = cutils_list_create (list->destructor, list->copier,
                                            list->comparator);

          if (clone->next == NULL)
            {
              cutils_list_destroy (&clone);

              return NULL;
            }
        }

      ptr_list = ptr_list->next;
      ptr_clone->next->prev = ptr_clone;
      ptr_clone = ptr_clone->next;
    }

  return clone;
}

CUTILS_BOOL
cutils_list_insert (CUTILS_LIST *node, const void *const value)
{
  CUTILS_LIST *new_node = NULL;

  cutils_error_clear ();

  if (node == NULL)
    {
      cutils_error_set ("Argument 'node' in cutils_list_insert is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (value == NULL)
    {
      cutils_error_set ("Argument 'value' in cutils_list_insert is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  new_node
      = cutils_list_create (node->destructor, node->copier, node->comparator);

  if (new_node == NULL)
    {
      return CUTILS_BOOL_FALSE;
    }

  new_node->value = node->copier (value);

  if (new_node->value == NULL)
    {
      cutils_error_set (
          "Failed to clone value into new node in cutils_list_insert.");

      cutils_list_destroy (&new_node);

      return CUTILS_BOOL_FALSE;
    }

  new_node->prev = node->prev;

  if (node->prev != NULL)
    {
      node->prev->next = new_node;
    }

  new_node->next = node;

  node->prev = new_node;

  return CUTILS_BOOL_TRUE;
}

CUTILS_LIST *
cutils_list_find (CUTILS_LIST *list, const void *const value)
{
  CUTILS_LIST *ptr = NULL;

  cutils_error_clear ();

  if (list == NULL)
    {
      cutils_error_set ("Argument 'list' in cutils_list_find is NULL.");

      return NULL;
    }

  if (value == NULL)
    {
      cutils_error_set ("Argument 'value' in cutils_list_find is NULL.");

      return NULL;
    }

  if (list->prev != NULL)
    {
      cutils_error_set ("List in cutils_list_find is not a root node.");

      return NULL;
    }

  ptr = list;

  while (ptr != NULL)
    {
      if (list->comparator (ptr->value, value) == CUTILS_BOOL_TRUE)
        {
          return ptr;
        }

      ptr = ptr->next;
    }

  return NULL;
}

CUTILS_BOOL
cutils_list_erase (CUTILS_LIST **node)
{
  cutils_error_clear ();

  if (node == NULL)
    {
      cutils_error_set ("Argument 'node' in cutils_list_erase is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (*node == NULL)
    {
      cutils_error_set ("Argument '*node' in cutils_list_erase is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if ((*node)->prev != NULL)
    {
      (*node)->prev->next = (*node)->next;
    }

  if ((*node)->next != NULL)
    {
      (*node)->next->prev = (*node)->prev;
    }

  (*node)->destructor (&(*node)->value);

  memset (*node, 0, sizeof (CUTILS_LIST));

  free (*node);

  *node = NULL;

  return CUTILS_BOOL_TRUE;
}

CUTILS_BOOL
cutils_list_prepend (CUTILS_LIST **list, const void *const value)
{
  CUTILS_LIST *new_node = NULL;

  cutils_error_clear ();

  if (list == NULL)
    {
      cutils_error_set ("Argument 'list' in cutils_list_prepend is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (*list == NULL)
    {
      cutils_error_set ("Argument '*list' in cutils_list_prepend is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (value == NULL)
    {
      cutils_error_set ("Argument 'value' in cutils_list_prepend is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if ((*list)->prev == NULL)
    {
      cutils_error_set (
          "Argument 'list' in cutils_list_prepend is not a root node.");

      return CUTILS_BOOL_FALSE;
    }

  new_node = cutils_list_create ((*list)->destructor, (*list)->copier,
                                 (*list)->comparator);

  if (new_node == NULL)
    {
      return CUTILS_BOOL_FALSE;
    }

  new_node->value = (*list)->copier (value);

  if (new_node->value == NULL)
    {
      cutils_error_set (
          "Failed to clone value into new node in cutils_list_prepend.");

      cutils_list_destroy (&new_node);

      return CUTILS_BOOL_FALSE;
    }

  new_node->next = *list;

  (*list)->prev = new_node;

  *list = new_node;

  return CUTILS_BOOL_TRUE;
}

CUTILS_BOOL
cutils_list_append (CUTILS_LIST *list, const void *const value)
{
  CUTILS_LIST *ptr = NULL;
  CUTILS_LIST *node = NULL;

  cutils_error_clear ();

  if (list == NULL)
    {
      cutils_error_set ("Argument 'list' in cutils_list_append is NULL");

      return CUTILS_BOOL_FALSE;
    }

  if (value == NULL)
    {
      cutils_error_set ("Argument 'value' in cutils_list_append is NULL");

      return CUTILS_BOOL_FALSE;
    }

  if (list->prev != NULL)
    {
      cutils_error_set (
          "Argument 'list' in cutils_list_append is not a root node.");

      return CUTILS_BOOL_FALSE;
    }

  node = cutils_list_create (list->destructor, list->copier, list->comparator);

  if (node == NULL)
    {
      return CUTILS_BOOL_FALSE;
    }

  node->value = list->copier (value);

  if (node->value == NULL)
    {
      cutils_error_set (
          "Failed to clone value into new node in cutils_list_append.");

      cutils_list_destroy (&node);

      return CUTILS_BOOL_FALSE;
    }

  ptr = list;

  while (ptr->next != NULL)
    {
      ptr = ptr->next;
    }

  ptr->next = node;
  node->prev = ptr;

  return CUTILS_BOOL_TRUE;
}

size_t
cutils_list_size (const CUTILS_LIST *const list)
{
  size_t count = 0;

  const CUTILS_LIST *ptr = NULL;

  cutils_error_clear ();

  if (list == NULL)
    {
      cutils_error_set ("Argument 'list' in cutils_list_size is NULL.");

      return 0;
    }

  if (list->prev == NULL)
    {
      cutils_error_set ("List in cutils_list_size is not a root node.");

      return 0;
    }

  ptr = list;

  while (ptr != NULL && ptr->value != NULL)
    {
      ++count;

      ptr = ptr->next;
    }

  return count;
}

void *
cutils_list_value_get (const CUTILS_LIST *const node)
{
  cutils_error_clear ();

  if (node == NULL)
    {
      cutils_error_set ("Argument 'node' in cutils_list_value_get is NULL.");

      return NULL;
    }

  if (node->value == NULL)
    {
      cutils_error_set ("Node's value is NULL in cutils_list_value_get.");

      return NULL;
    }

  return node->value;
}

CUTILS_BOOL
cutils_list_value_set (CUTILS_LIST *node, const void *const value)
{
  void *new_value = NULL;

  cutils_error_clear ();

  if (node == NULL)
    {
      cutils_error_set ("Argument 'node' in cutils_list_value_set is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (value == NULL)
    {
      cutils_error_set ("Argument 'value' in cutils_list_value_set is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  new_value = node->copier (value);

  if (new_value == NULL)
    {
      cutils_error_set (
          "Failed to copy value into new value in cutils_list_value_set.");

      return CUTILS_BOOL_FALSE;
    }

  node->destructor (&node->value);

  node->value = new_value;

  return CUTILS_BOOL_TRUE;
}

const void *
cutils_list_value_front (const CUTILS_LIST *const list)
{
  cutils_error_clear ();

  if (list == NULL)
    {
      cutils_error_set ("Argument 'list' in cutils_list_value_front is NULL.");

      return NULL;
    }

  if (list->prev != NULL)
    {
      cutils_error_set ("List in cutils_list_value_front is not a root node.");

      return NULL;
    }

  if (list->value == NULL)
    {
      cutils_error_set ("Value for node in cutils_list_value_front is NULL.");

      return NULL;
    }

  return list->value;
}

const void *
cutils_list_value_back (const CUTILS_LIST *const list)
{
  const CUTILS_LIST *ptr = NULL;

  cutils_error_clear ();

  if (list == NULL)
    {
      cutils_error_set ("Argument 'list' in cutils_list_value_back is NULL.");

      return NULL;
    }

  if (list->prev != NULL)
    {
      cutils_error_set ("List in cutils_list_value_back is not a root node.");

      return NULL;
    }

  ptr = list;

  while (ptr->next != NULL)
    {
      ptr = ptr->next;
    }

  if (ptr->value == NULL)
    {
      cutils_error_set ("Value for node in cutils_list_value_back is NULL.");

      return NULL;
    }

  return ptr->value;
}

CUTILS_LIST *
cutils_list_node_next (CUTILS_LIST *node)
{
  cutils_error_clear ();

  if (node == NULL)
    {
      cutils_error_set ("Argument 'node' in cutils_list_node_next is NULL.");

      return NULL;
    }

  return node->next;
}

CUTILS_LIST *
cutils_list_node_prev (CUTILS_LIST *node)
{
  cutils_error_clear ();

  if (node == NULL)
    {
      cutils_error_set ("Argument 'node' in cutils_list_node_prev is NULL.");

      return NULL;
    }

  return node->prev;
}

CUTILS_BOOL
cutils_list_remove_front (CUTILS_LIST **list)
{
  CUTILS_LIST *ptr = NULL;

  cutils_error_clear ();

  if (list == NULL)
    {
      cutils_error_set (
          "Argument 'list' in cutils_list_remove_front is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (*list == NULL)
    {
      cutils_error_set (
          "Argument '*list' in cutils_list_remove_front is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if ((*list)->prev != NULL)
    {
      cutils_error_set (
          "List in cutils_list_remove_front is not a root node.");

      return CUTILS_BOOL_FALSE;
    }

  ptr = (*list)->next;

  if (ptr != NULL)
    {
      ptr->prev = NULL;
    }

  if ((*list)->value != NULL)
    {
      (*list)->destructor ((*list)->value);
    }

  memset (*list, 0, sizeof (CUTILS_LIST));

  free (*list);

  *list = ptr;

  return CUTILS_BOOL_TRUE;
}

CUTILS_BOOL
cutils_list_remove_back (CUTILS_LIST **list)
{
  CUTILS_LIST *ptr = NULL;

  cutils_error_clear ();

  if (list == NULL)
    {
      cutils_error_set ("Argument 'list' in cutils_list_remove_back is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (*list == NULL)
    {
      cutils_error_set (
          "Argument '*list' in cutils_list_remove_back is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if ((*list)->prev != NULL)
    {
      cutils_error_set ("List in cutils_list_remove_back is not a root node.");

      return CUTILS_BOOL_FALSE;
    }

  ptr = *list;

  while (ptr->next != NULL)
    {
      ptr = ptr->next;
    }

  if (ptr->prev != NULL)
    {
      ptr->prev->next = NULL;
    }
  else
    {
      *list = NULL;
    }

  if (ptr->value != NULL)
    {
      ptr->destructor (ptr->value);
    }

  memset (ptr, 0, sizeof (CUTILS_LIST));

  free (ptr);

  return CUTILS_BOOL_TRUE;
}

CUTILS_MAP *
cutils_map_create (const CUTILS_MAP_DESTRUCTOR destructor,
                   const CUTILS_MAP_COPIER copier,
                   const CUTILS_MAP_COMPARATOR comparator)
{
  CUTILS_MAP *map = NULL;

  cutils_error_clear ();

  if (destructor == NULL)
    {
      cutils_error_set ("Argument 'destructor' in cutils_map_create is NULL.");

      return NULL;
    }

  if (copier == NULL)
    {
      cutils_error_set ("Argument 'copier' in cutils_map_create is NULL.");

      return NULL;
    }

  if (comparator == NULL)
    {
      cutils_error_set ("Argument 'comparator' in cutils_map_create is NULL.");

      return NULL;
    }

  map = malloc (sizeof (CUTILS_MAP));

  if (map == NULL)
    {
      cutils_error_set (
          "Failed to allocate memory for map in cutils_map_create.");

      return NULL;
    }

  memset (map, 0, sizeof (CUTILS_MAP));

  map->destructor = destructor;

  map->copier = copier;

  map->comparator = comparator;

  return map;
}

void
cutils_map_destroy (CUTILS_MAP **map)
{
  CUTILS_MAP *ptr = NULL;

  cutils_error_clear ();

  if (map == NULL || *map == NULL)
    {
      return;
    }

  ptr = *map;

  while (ptr != NULL)
    {
      CUTILS_MAP *next = ptr->next;

      if (ptr->value != NULL)
        {
          ptr->destructor (&ptr->value);
          cutils_string_destroy (&ptr->key);
        }

      memset (ptr, 0, sizeof (CUTILS_MAP));

      free (ptr);

      ptr = next;
    }

  *map = NULL;
}

CUTILS_MAP *
cutils_map_clone (const CUTILS_MAP *const map)
{
  const CUTILS_MAP *ptr_map = NULL;
  CUTILS_MAP *ptr_clone = NULL;
  CUTILS_MAP *clone = NULL;

  cutils_error_clear ();

  if (map == NULL)
    {
      cutils_error_set ("Argument 'map' in cutils_map_clone is NULL.");

      return NULL;
    }

  if (map->prev != NULL)
    {
      cutils_error_set ("Argument 'map' is not a root node.");

      return NULL;
    }

  clone = cutils_map_create (map->destructor, map->copier, map->comparator);

  if (clone == NULL)
    {
      return NULL;
    }

  ptr_map = map;

  ptr_clone = clone;

  while (ptr_map != NULL)
    {
      clone->value = map->copier (map->value);

      if (clone->value == NULL)
        {
          cutils_error_set ("Cloned value is NULL in cutils_map_clone.");

          cutils_map_destroy (&clone);

          return NULL;
        }

      clone->key = cutils_string_clone (map->key);

      if (clone->key == NULL)
        {
          return NULL;
        }

      if (ptr_map->next != NULL)
        {
          clone->next = cutils_map_create (map->destructor, map->copier,
                                           map->comparator);

          if (clone->next == NULL)
            {
              cutils_map_destroy (&clone);

              return NULL;
            }
        }

      ptr_map = ptr_map->next;
      ptr_clone->next->prev = ptr_clone;
      ptr_clone = ptr_clone->next;
    }

  return clone;
}

CUTILS_BOOL
cutils_map_insert (CUTILS_MAP *map, const char *const key,
                   const void *const value)
{
  CUTILS_MAP *new_map = NULL;
  CUTILS_MAP *ptr = NULL;

  cutils_error_clear ();

  if (map == NULL)
    {
      cutils_error_set ("Argument 'map' in cutils_map_insert is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (value == NULL)
    {
      cutils_error_set ("Argument 'value' in cutils_map_insert is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (map->prev != NULL)
    {
      cutils_error_set ("Map in cutils_map_insert is not a root node.");

      return CUTILS_BOOL_FALSE;
    }

  if (key == NULL)
    {
      cutils_error_set ("Argument 'key' in cutils_map_insert is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (cutils_map_get (map, key) != NULL)
    {
      cutils_error_set (
          "Map already contains the key/value pair for value '%s'.", key);

      return CUTILS_BOOL_FALSE;
    }

  new_map = cutils_map_create (map->destructor, map->copier, map->comparator);

  if (new_map == NULL)
    {
      return CUTILS_BOOL_FALSE;
    }

  new_map->value = map->copier (value);

  if (new_map->value == NULL)
    {
      cutils_error_set (
          "Failed to clone value into new map in cutils_map_insert.");

      cutils_map_destroy (&new_map);

      return CUTILS_BOOL_FALSE;
    }

  new_map->key = cutils_string_create_from_text (key);

  if (new_map->key == NULL)
    {
      cutils_map_destroy (&new_map);

      return CUTILS_BOOL_FALSE;
    }

  ptr = map;

  while (ptr->next != NULL)
    {
      ptr = ptr->next;
    }

  ptr->next = new_map;

  new_map->prev = ptr;

  return CUTILS_BOOL_TRUE;
}

CUTILS_BOOL
cutils_map_erase (CUTILS_MAP **map, const char *const key)
{
  cutils_error_clear ();

  if (map == NULL)
    {
      cutils_error_set ("Argument 'map' in cutils_map_erase is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (*map == NULL)
    {
      cutils_error_set ("Argument '*map' in cutils_map_erase is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (key == NULL)
    {
      cutils_error_set ("Argument 'key' in cutils_map_erase is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if ((*map)->prev != NULL)
    {
      (*map)->prev->next = (*map)->next;
    }

  if ((*map)->next != NULL)
    {
      (*map)->next->prev = (*map)->prev;
    }

  (*map)->destructor (&(*map)->value);

  cutils_string_destroy (&(*map)->key);

  memset (*map, 0, sizeof (CUTILS_MAP));

  free (*map);

  *map = NULL;

  return CUTILS_BOOL_TRUE;
}

size_t
cutils_map_size (const CUTILS_MAP *const map)
{
  size_t count = 0;

  const CUTILS_MAP *ptr = NULL;

  cutils_error_clear ();

  if (map == NULL)
    {
      cutils_error_set ("Argument 'map' in cutils_map_size is NULL.");

      return 0;
    }

  if (map->prev == NULL)
    {
      cutils_error_set ("Map in cutils_map_size is not a root node.");

      return 0;
    }

  ptr = map;

  while (ptr != NULL && ptr->value != NULL)
    {
      ++count;

      ptr = ptr->next;
    }

  return count;
}

void *
cutils_map_get (const CUTILS_MAP *const map, const char *const key)
{
  CUTILS_MAP *ptr = NULL;

  cutils_error_clear ();

  if (map == NULL)
    {
      cutils_error_set ("Argument 'map' in cutils_map_get is NULL.");

      return NULL;
    }

  if (key == NULL)
    {
      cutils_error_set ("Argument 'key' in cutils_map_get is NULL.");

      return NULL;
    }

  if (map->prev != NULL)
    {
      cutils_error_set ("Map in cutils_map_get is not a root node.");

      return NULL;
    }

  while (ptr != NULL)
    {
      if (cutils_string_compare_with_text (ptr->key, key) == CUTILS_BOOL_TRUE)
        {
          return ptr->value;
        }

      ptr = ptr->next;
    }

  return NULL;
}

CUTILS_BOOL
cutils_map_set (CUTILS_MAP *map, const char *const key,
                const void *const value)
{
  void *new_value = NULL;
  CUTILS_MAP *ptr = NULL;

  cutils_error_clear ();

  if (map == NULL)
    {
      cutils_error_set ("Argument 'map' in cutils_map_set is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (key == NULL)
    {
      cutils_error_set ("Argument 'key' in cutils_map_set is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (value == NULL)
    {
      cutils_error_set ("Argument 'value' in cutils_map_set is NULL.");

      return CUTILS_BOOL_FALSE;
    }

  if (map->prev != NULL)
    {
      cutils_error_set ("Map in cutils_map_set is not a root node.");

      return CUTILS_BOOL_FALSE;
    }

  new_value = map->copier (value);

  if (new_value == NULL)
    {
      cutils_error_set (
          "Failed to copy value into new value in cutils_map_set.");

      return CUTILS_BOOL_FALSE;
    }

  ptr = map;

  while (ptr != NULL)
    {
      if (cutils_string_compare_with_text (ptr->key, key) == CUTILS_BOOL_TRUE)
        {
          break;
        }

      ptr = ptr->next;
    }

  if (ptr == NULL)
    {
      cutils_error_set (
          "Could not find key/value pair in cutils_map_set using key '%s'.",
          key);

      map->destructor (&new_value);

      return CUTILS_BOOL_FALSE;
    }

  ptr->destructor (&ptr->value);

  ptr->value = new_value;

  return CUTILS_BOOL_TRUE;
}

unsigned int
cutils_jenkins_hash (const unsigned char *const data, const size_t size)
{
  size_t index = 0;

  unsigned int hash = 0;

  cutils_error_clear ();

  while (index != size)
    {
      hash += data[index++];

      hash += hash << 10;

      hash ^= hash >> 6;
    }

  hash += hash << 3;

  hash ^= hash >> 11;

  hash += hash << 15;

  return hash;
}

double
cutils_random_double (const double min, const double max)
{
  cutils_error_clear ();

  srand ((unsigned int)cutils_get_ticks ());

  return (double)(min + rand () % (int)(max + 1.0 - min));
}

int
cutils_random_int (const int min, const int max)
{
  cutils_error_clear ();

  srand ((unsigned int)cutils_get_ticks ());

  return min + rand () % (max + 1 - min);
}

unsigned long
cutils_get_ticks (void)
{
  unsigned long ticks = 0;

#ifdef CUTILS_OS_UNIX_LINUX_DARWIN
  struct timeval tv;

  cutils_error_clear ();

  if (gettimeofday (&tv, NULL) != 0)
    {
      cutils_error_set ("Failed to get ticks in cutils_get_ticks.");

      return 0;
    }

  ticks = (unsigned long)tv.tv_sec * 1000 + (unsigned long)tv.tv_usec / 1000;
#endif /* CUTILS_OS_UNIX_LINUX_DARWIN */

#ifdef CUTILS_OS_WINDOWS
  cutils_error_clear ();

  ticks = (unsigned long)timeGetTime ();
#endif /* CUTILS_OS_WINDOW */

  return ticks;
}

CUTILS_STRING *
cutils_base64_encode (const unsigned char *const data, const size_t size)
{
  size_t index = 0;

  CUTILS_STRING *string = NULL;

  cutils_error_clear ();

  if (data == NULL)
    {
      cutils_error_set ("Argument 'data' in cutils_base64_encode is NULL.");

      return NULL;
    }

  string = cutils_string_create ();

  if (string == NULL)
    {
      cutils_error_set (
          "Failed to create CUTILS_STRING in cutils_base64_encode.");

      return NULL;
    }

  for (index = 0; index < size; index += 3)
    {
      int combined = 0;

      char a = '\0';

      char b = '\0';

      char c = '\0';

      char d = '\0';

      if (size - index == 2)
        {
          combined = ((data[index] << 16) & CUTILS_BITMASK (8, 16))
                     | ((data[index + 1] << 8) & CUTILS_BITMASK (8, 8));

          a = (combined >> 18) & CUTILS_BITMASK (6, 0);

          b = (combined >> 12) & CUTILS_BITMASK (6, 0);

          c = (combined >> 6) & CUTILS_BITMASK (6, 0);

          cutils_string_append (&string, base64_lookup_encode[(int)a]);

          cutils_string_append (&string, base64_lookup_encode[(int)b]);

          cutils_string_append (&string, base64_lookup_encode[(int)c]);

          cutils_string_append (&string, '=');
        }
      else if (size - index == 1)
        {
          combined = ((data[index] << 16) & CUTILS_BITMASK (8, 16));

          a = (combined >> 18) & CUTILS_BITMASK (6, 0);

          b = (combined >> 12) & CUTILS_BITMASK (6, 0);

          cutils_string_append (&string, base64_lookup_encode[(int)a]);

          cutils_string_append (&string, base64_lookup_encode[(int)b]);

          cutils_string_append (&string, '=');

          cutils_string_append (&string, '=');
        }
      else
        {
          combined = ((data[index] << 16) & CUTILS_BITMASK (8, 16))
                     | ((data[index + 1] << 8) & CUTILS_BITMASK (8, 8))
                     | (data[index + 2] & CUTILS_BITMASK (8, 0));

          a = (combined >> 18) & CUTILS_BITMASK (6, 0);

          b = (combined >> 12) & CUTILS_BITMASK (6, 0);

          c = (combined >> 6) & CUTILS_BITMASK (6, 0);

          d = combined & CUTILS_BITMASK (6, 0);

          cutils_string_append (&string, base64_lookup_encode[(int)a]);

          cutils_string_append (&string, base64_lookup_encode[(int)b]);

          cutils_string_append (&string, base64_lookup_encode[(int)c]);

          cutils_string_append (&string, base64_lookup_encode[(int)d]);
        }
    }

  return string;
}

CUTILS_BINARY *
cutils_base64_decode (const char *const string)
{
  size_t index = 0;

  CUTILS_BINARY *binary = NULL;

  cutils_error_clear ();

  if (string == NULL)
    {
      cutils_error_set ("Argument 'string' in cutils_base64_decode is NULL.");

      return NULL;
    }

  binary = cutils_binary_create ();

  if (binary == NULL)
    {
      cutils_error_set (
          "Failed to create CUTILS_BINARY in cutils_base64_decode.");

      return NULL;
    }

  for (index = 0; index < strlen (string); index += 4)
    {
      int combined = 0;

      char a = base64_lookup_decode[(int)string[index]];

      char b = base64_lookup_decode[(int)string[index + 1]];

      char c = base64_lookup_decode[(int)string[index + 2]];

      char d = base64_lookup_decode[(int)string[index + 3]];

      unsigned char e = 0;

      unsigned char f = 0;

      unsigned char g = 0;

      if (string[index + 2] == '=' && string[index + 3] == '=')
        {
          combined = (a << 18) | (b << 12) | (c << 6) | d;

          e = (unsigned char)((combined >> 16) & CUTILS_BITMASK (8, 0));

          cutils_binary_append (&binary, e);
        }
      else if (string[index + 3] == '=')
        {
          combined = (a << 18) | (b << 12) | (c << 6) | d;

          e = (unsigned char)((combined >> 16) & CUTILS_BITMASK (8, 0));
          f = (unsigned char)((combined >> 8) & CUTILS_BITMASK (8, 0));

          cutils_binary_append (&binary, e);

          cutils_binary_append (&binary, f);
        }
      else
        {
          combined = (a << 18) | (b << 12) | (c << 6) | d;

          e = (unsigned char)((combined >> 16) & CUTILS_BITMASK (8, 0));
          f = (unsigned char)((combined >> 8) & CUTILS_BITMASK (8, 0));
          g = (unsigned char)((combined >> 0) & CUTILS_BITMASK (8, 0));

          cutils_binary_append (&binary, e);

          cutils_binary_append (&binary, f);

          cutils_binary_append (&binary, g);
        }
    }

  return binary;
}
