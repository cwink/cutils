#ifndef CUTILS_H
#define CUTILS_H

#include <stddef.h>
#include <stdio.h>

#define CUTILS_PI 3.14159265359

#if defined(__unix__) || defined(__linux__)
#define CUTILS_OS_UNIX_LINUX
#endif /* __unix__ || __linux__ */

#ifdef __APPLE__
#define CUTILS_OS_APPLE
#endif /* __APPLE__ */

#ifdef _WIN32
#define CUTILS_OS_WINDOWS _WIN32
#endif /* _WIN32 */

#if defined(CUTILS_OS_UNIX_LINUX) || defined(CUTILS_OS_APPLE)
#define CUTILS_OS_UNIX_LINUX_DARWIN
#endif /* CUTILS_OS_UNIX_LINUX || CUTILS_OS_APPLE */

#define CUTILS_BITMASK(NUM_BITS, SHIFT_LEFT)                                  \
  (((1 << NUM_BITS) - 1) << SHIFT_LEFT)

typedef enum cutils_bool_t
{
  CUTILS_BOOL_FALSE = 0,
  CUTILS_BOOL_TRUE = 1,
  CUTILS_BOOL_MAX = 2
} CUTILS_BOOL;

typedef enum cutils_file_open_t
{
  CUTILS_FILE_OPEN_READ = 0,
  CUTILS_FILE_OPEN_WRITE = 1,
  CUTILS_FILE_OPEN_APPEND = 2,
  CUTILS_FILE_OPEN_MAX = 3
} CUTILS_FILE_OPEN;

typedef void (*CUTILS_LIST_DESTRUCTOR) (void **);

typedef void *(*CUTILS_LIST_COPIER) (const void *const);

typedef CUTILS_BOOL (*CUTILS_LIST_COMPARATOR) (const void *const,
                                               const void *const);

typedef void (*CUTILS_MAP_DESTRUCTOR) (void **);

typedef void *(*CUTILS_MAP_COPIER) (const void *const);

typedef CUTILS_BOOL (*CUTILS_MAP_COMPARATOR) (const void *const,
                                              const void *const);

typedef struct cutils_string_t CUTILS_STRING;

typedef struct cutils_binary_t CUTILS_BINARY;

typedef struct cutils_list_t CUTILS_LIST;

typedef struct cutils_map_t CUTILS_MAP;

const char *cutils_error_get (void);

CUTILS_BOOL cutils_error_triggered (void);

CUTILS_STRING *cutils_string_create (void);

CUTILS_STRING *cutils_string_create_from_text (const char *const text);

void cutils_string_destroy (CUTILS_STRING **string);

CUTILS_STRING *cutils_string_clone (CUTILS_STRING *string);

CUTILS_BOOL cutils_string_insert (CUTILS_STRING **string, const size_t index,
                                  const char value);

CUTILS_BOOL cutils_string_append (CUTILS_STRING **string, const char value);

CUTILS_BOOL cutils_string_prepend (CUTILS_STRING **string, const char value);

CUTILS_BOOL cutils_string_erase (CUTILS_STRING **string, const size_t index);

const char *cutils_string_get (CUTILS_STRING *string);

CUTILS_BOOL cutils_string_reset (CUTILS_STRING **string,
                                 const char *const text);

size_t cutils_string_length (CUTILS_STRING *string);

CUTILS_BOOL cutils_string_compare (const CUTILS_STRING *const left,
                                   const CUTILS_STRING *const right);

CUTILS_BOOL cutils_string_compare_with_text (const CUTILS_STRING *const left,
                                             const char *const right);

CUTILS_BINARY *cutils_binary_create (void);

CUTILS_BINARY *cutils_binary_create_from_data (const unsigned char *const data,
                                               const size_t size);

void cutils_binary_destroy (CUTILS_BINARY **binary);

CUTILS_BINARY *cutils_binary_clone (CUTILS_BINARY *binary);

CUTILS_BOOL cutils_binary_insert (CUTILS_BINARY **binary, const size_t index,
                                  const unsigned char value);

CUTILS_BOOL cutils_binary_append (CUTILS_BINARY **binary,
                                  const unsigned char value);

CUTILS_BOOL cutils_binary_prepend (CUTILS_BINARY **binary,
                                   const unsigned char value);

CUTILS_BOOL cutils_binary_erase (CUTILS_BINARY **binary, const size_t index);

const unsigned char *cutils_binary_get (CUTILS_BINARY *binary);

CUTILS_BOOL cutils_binary_reset (CUTILS_BINARY **binary,
                                 const unsigned char *const data,
                                 const size_t size);

size_t cutils_binary_size (CUTILS_BINARY *binary);

CUTILS_BOOL cutils_binary_compare (const CUTILS_BINARY *const left,
                                   const CUTILS_BINARY *const right);

CUTILS_BOOL cutils_binary_compare_with_data (const CUTILS_BINARY *const left,
                                             const unsigned char *const right,
                                             const size_t size);

FILE *cutils_file_open (const char *const file_name,
                        const CUTILS_FILE_OPEN type);

CUTILS_BOOL cutils_file_close (FILE **file);

int cutils_file_get_char (FILE *file);

CUTILS_BOOL cutils_file_get_string (FILE *file, char *buffer,
                                    const size_t count);

int cutils_file_peek_char (FILE *file);

CUTILS_BOOL cutils_file_peek_string (FILE *file, char *buffer,
                                     const size_t count);

CUTILS_BOOL cutils_file_put_char (FILE *file, const int ch);

CUTILS_BOOL cutils_file_put_string (FILE *file, const char *const string);

CUTILS_BOOL cutils_file_read (FILE *file, void *buffer, const size_t size,
                              const size_t count);

CUTILS_BOOL cutils_file_write (FILE *file, const void *const buffer,
                               const size_t size, const size_t count);

size_t cutils_file_size (FILE *file);

CUTILS_LIST *cutils_list_create (const CUTILS_LIST_DESTRUCTOR destructor,
                                 const CUTILS_LIST_COPIER copier,
                                 const CUTILS_LIST_COMPARATOR comparator);

void cutils_list_destroy (CUTILS_LIST **list);

CUTILS_LIST *cutils_list_clone (const CUTILS_LIST *const list);

CUTILS_BOOL cutils_list_insert (CUTILS_LIST *node, const void *const value);

CUTILS_LIST *cutils_list_find (CUTILS_LIST *list, const void *const value);

CUTILS_BOOL cutils_list_erase (CUTILS_LIST **node);

CUTILS_BOOL cutils_list_prepend (CUTILS_LIST **list, const void *const value);

CUTILS_BOOL cutils_list_append (CUTILS_LIST *list, const void *const value);

size_t cutils_list_size (const CUTILS_LIST *const list);

void *cutils_list_value_get (const CUTILS_LIST *const node);

CUTILS_BOOL cutils_list_value_set (CUTILS_LIST *node, const void *const value);

const void *cutils_list_value_front (const CUTILS_LIST *const list);

const void *cutils_list_value_back (const CUTILS_LIST *const list);

CUTILS_LIST *cutils_list_node_next (CUTILS_LIST *node);

CUTILS_LIST *cutils_list_node_prev (CUTILS_LIST *node);

CUTILS_BOOL cutils_list_remove_front (CUTILS_LIST **list);

CUTILS_BOOL cutils_list_remove_back (CUTILS_LIST **list);

CUTILS_MAP *cutils_map_create (const CUTILS_MAP_DESTRUCTOR destructor,
                               const CUTILS_MAP_COPIER copier,
                               const CUTILS_MAP_COMPARATOR comparator);

void cutils_map_destroy (CUTILS_MAP **map);

CUTILS_MAP *cutils_map_clone (const CUTILS_MAP *const map);

CUTILS_BOOL cutils_map_insert (CUTILS_MAP *map, const char *const key,
                               const void *const value);

void *cutils_map_find (const CUTILS_MAP *const map, const char *const key);

CUTILS_BOOL cutils_map_erase (CUTILS_MAP **map, const char *const key);

size_t cutils_map_size (const CUTILS_MAP *const map);

void *cutils_map_get (const CUTILS_MAP *const map, const char *const key);

CUTILS_BOOL cutils_map_set (CUTILS_MAP *map, const char *const key,
                            const void *const value);

unsigned int cutils_jenkins_hash (const unsigned char *const data,
                                  const size_t size);

double cutils_random_double (const double min, const double max);

int cutils_random_int (const int min, const int max);

unsigned long cutils_get_ticks (void);

CUTILS_STRING *cutils_base64_encode (const unsigned char *const data,
                                     const size_t size);

CUTILS_BINARY *cutils_base64_decode (const char *const string);

#endif /* CUTILS_H */
