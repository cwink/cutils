PROJECT := cutils

CC := clang

CFLAGS_DEBUG := $(CFLAGS) -std=c89 -Weverything -pedantic-errors -O0 -g
LDFLAGS_DEBUG := $(LDFLAGS)

CFLAGS_RELEASE := $(CFLAGS) -std=c89 -Weverything -pedantic-errors -O2
LDFLAGS_RELEASE := $(LDFLAGS)

all : build/$(PROJECT)_debug.x build/$(PROJECT)_release.x

build/$(PROJECT)_debug.x : build/$(PROJECT)_debug.o build/cutils_src_debug.o
	$(CC) $(CFLAGS_DEBUG) -o $@ $^ $(LDFLAGS_DEBUG)

build/$(PROJECT)_debug.o : $(PROJECT).c
	$(CC) $(CFLAGS_DEBUG) -c -o $@ $<

build/cutils_src_debug.o : source/cutils.c
	$(CC) $(CFLAGS_DEBUG) -c -o $@ $<

build/$(PROJECT)_release.x : build/$(PROJECT)_release.o build/cutils_src_release.o
	$(CC) $(CFLAGS_RELEASE) -o $@ $^ $(LDFLAGS_RELEASE)

build/$(PROJECT)_release.o : $(PROJECT).c
	$(CC) $(CFLAGS_RELEASE) -c -o $@ $<

build/cutils_src_release.o : source/cutils.c
	$(CC) $(CFLAGS_RELEASE) -c -o $@ $<

clean :
	rm -rf build/*.o
	rm -rf build/*.x

run :
	build/$(PROJECT)_debug.x

valgrind :
	valgrind --suppressions=.valgrind --leak-check=full build/$(PROJECT)_debug.x

format :
	clang-format -i -style=gnu include/*.h
	clang-format -i -style=gnu source/*.c
